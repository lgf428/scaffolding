package com.sky.base.controller;

import com.sky.base.security.pojo.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 获取登录用户信息
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/1
 */
@Slf4j
public class BaseController {

    /**
     * 获取当前登录用户信息
     * @return
     */
    public LoginUser getLoginUser(){
        return (LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    /**
     * 获取当前登录用户id
     * @return
     */
    public Long getUserId(){
        return getLoginUser().getUser().getId();
    }

    /**
     * 获取当前登录用户登录名
     * @return
     */
    public String getUsername(){
        return getLoginUser().getUser().getUsername();
    }

}
