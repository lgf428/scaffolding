package com.sky.base.security;

import org.springframework.security.core.Authentication;

/**
 * 身份验证信息
 * 
 * @author 尹稳健~
 */
public class AuthenticationContextHolder
{
    // 获取当前登录用户的信息
    private static final ThreadLocal<Authentication> contextHolder = new ThreadLocal<>();

    public static Authentication getContext()
    {
        return contextHolder.get();
    }

    public static void setContext(Authentication context)
    {
        contextHolder.set(context);
    }

    public static void clearContext()
    {
        contextHolder.remove();
    }
}
