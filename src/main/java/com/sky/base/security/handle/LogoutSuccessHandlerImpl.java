package com.sky.base.security.handle;

import com.sky.base.security.pojo.LoginUser;
import com.sky.base.security.utils.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * 登出处理类
 *
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/14
 */
@Component
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler {

    @Autowired
    private TokenUtil tokenUtil;

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        LoginUser loginUser = tokenUtil.getLoginUser(request);
        if (!Objects.isNull(loginUser)){
            String token = tokenUtil.getToken(request);
            tokenUtil.delLoginUser(token);
        }
    }
}
