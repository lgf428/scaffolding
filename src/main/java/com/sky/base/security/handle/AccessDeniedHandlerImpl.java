package com.sky.base.security.handle;

import com.alibaba.fastjson2.JSON;
import com.sky.base.constant.HttpStatus;
import com.sky.utils.R;
import com.sky.utils.ServletUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 权限不足处理类
 * @author 尹稳健~
 */
@Component
public class AccessDeniedHandlerImpl implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {

        String json = JSON.toJSONString(R.fail(HttpStatus.FORBIDDEN,"权限不足，请和管理员联系"));
        //处理异常
        ServletUtils.renderString(response,json);
    }
}
