package com.sky.base.constant;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/20
 */
public class DataStatus {
    /** 正常 */
    public static final Short NORMAL = 0;
    /** 删除或者 错误 */
    public static final Short REMOVE = 1;
}
