package com.sky.api.sys.param.request;

import com.sky.model.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 添加系统用户入参模型
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/1
 */
@Data
@ApiModel("添加系统用户入参模型")
public class InsertSysUserModel implements Serializable {

    @ApiModelProperty(value = "用户账号名称",required = true)
    @NotEmpty(message = "用户账号名称不能为空")
    private String username;

    @ApiModelProperty(value = "密码",required = true)
    @NotEmpty(message = "密码不能为空")
    private String password;

    @ApiModelProperty(value = "用户昵称",required = true)
    private String nickName;

    @ApiModelProperty(value = "用户头像",required = true)
    @NotEmpty(message = "用户头像不能为空")
    private String avatar;

    @ApiModelProperty(value = "用户性别",required = true)
    @NotNull(message = "用户性别不能为空")
    private Short sex;

    @ApiModelProperty(value = "用户邮箱",required = true)
    @NotEmpty(message = "用户邮箱不能为空")
    private String email;

    @ApiModelProperty(value = "手机号码",required = true)
    @NotEmpty(message = "手机号码不能为空")
    private String phone;

    @ApiModelProperty(value = "用户等级",required = true)
    @NotNull(message = "用户等级不能为空")
    private Short hierarchy;

    @ApiModelProperty(value = "备注")
    private String remark;

}
