package com.sky.api.sys.param.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 用户登录对象
 * 
 * @author 尹稳健~
 */
@ApiModel("用户登录入参模型")
@Data
public class LoginBody {
    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名",required = true)
    @NotEmpty(message = "用户名不能为空")
    private String username;

    /**
     * 用户密码
     */
    @ApiModelProperty(value = "用户密码",required = true)
    @NotEmpty(message = "用户密码不能为空")
    private String password;

}
