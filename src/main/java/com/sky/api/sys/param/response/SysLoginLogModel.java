package com.sky.api.sys.param.response;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/20
 */
@ApiModel("登录日志返回模型")
@Data
public class SysLoginLogModel implements Serializable {
    @ApiModelProperty("id")
    private Long id;
    @ApiModelProperty("状态 0：成功 1:失败")
    private Short status;
    @ApiModelProperty("IP地址")
    private String ip;
    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("登录时间")
    private LocalDateTime loginTime;
}
