package com.sky.api.sys.param.request;

import com.sky.model.PageQueryModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/3
 */
@Data
@ApiModel("系统用户分页查询入参模型")
public class PageQuerySysUser extends PageQueryModel implements Serializable {
    @ApiModelProperty("用户名称")
    private String nickName;
    @ApiModelProperty("手机号码")
    private String phone;
    @ApiModelProperty("状态")
    private Short status;
    @ApiModelProperty("创建时间的开始时间")
    private LocalDateTime beginTime;
    @ApiModelProperty("创建时间的结束时间")
    private LocalDateTime endTime;
}
