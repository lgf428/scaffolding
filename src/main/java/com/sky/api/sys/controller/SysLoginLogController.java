package com.sky.api.sys.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sky.api.sys.param.response.SysLoginLogModel;
import com.sky.model.PageQueryModel;
import com.sky.model.SysLoginLog;
import com.sky.service.SysLoginLogService;
import com.sky.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/20
 */
@RestController
@RequestMapping("/manage/sys/loginLog")
@Api(tags = "系统设置-登录日志")
public class SysLoginLogController {

    @Autowired
    private SysLoginLogService sysLoginLogService;

    @ApiOperation("登录日志分页查询")
    @PostMapping("/pageQuery")
    public R<IPage<SysLoginLogModel>> pageQuery(@RequestBody PageQueryModel pageQueryModel){
        IPage<SysLoginLog> iPage = sysLoginLogService.pageQuery(pageQueryModel);
        System.out.println(iPage.getRecords());
        IPage<SysLoginLogModel> page = new Page<>();
        BeanUtils.copyProperties(iPage,page,"records");
        List<SysLoginLogModel> newRecords = iPage.getRecords().stream().map(sysLoginLog -> {
            Class<SysLoginLogModel> sysLoginLogClass = SysLoginLogModel.class;
            SysLoginLogModel loginLog = null;
            try {
                loginLog = sysLoginLogClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
            BeanUtils.copyProperties(sysLoginLog, loginLog);
            return loginLog;
        }).collect(Collectors.toList());
        page.setRecords(newRecords);
        return R.ok(page);
    }

}
