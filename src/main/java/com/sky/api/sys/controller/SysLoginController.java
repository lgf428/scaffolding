package com.sky.api.sys.controller;

import com.sky.base.constant.Constants;
import com.sky.utils.R;
import com.sky.api.sys.param.request.LoginBody;
import com.sky.base.security.service.SysLoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/9
 */
@Api(tags = "系统管理-登录")
@RestController
public class SysLoginController {

    @Autowired
    private SysLoginService sysLoginService;

    @ApiOperation("登录接口")
    @PostMapping("/login")
    public R login(@RequestBody @Valid LoginBody loginBody){
        // Todo 校验验证码

        String token = sysLoginService.login(loginBody.getUsername(), loginBody.getPassword());
        Map<String, Object> map = new HashMap<>();
        map.put(Constants.TOKEN,token);
        return R.ok(map,"登录成功");
    }
}
