package com.sky.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sky.api.sys.param.request.PageQuerySysUser;
import com.sky.model.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/9
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
    /**
     * 分页查询系统用户
     * @param iPage
     * @param pageQuerySysUser
     * @return
     */
    IPage<SysUser> pageQuery(IPage<SysUser> iPage,PageQuerySysUser pageQuerySysUser);
}
