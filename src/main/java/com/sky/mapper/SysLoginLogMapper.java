package com.sky.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sky.model.SysLoginLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/20
 */
@Mapper
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {
}
