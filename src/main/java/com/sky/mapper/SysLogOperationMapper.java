package com.sky.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sky.model.SysLogOperation;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/5
 */
@Mapper
public interface SysLogOperationMapper extends BaseMapper<SysLogOperation> {
}
