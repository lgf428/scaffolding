package com.sky.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sky.api.sys.param.request.PageQuerySysUser;
import com.sky.model.SysUser;
import org.springframework.stereotype.Service;

/**
 * 系统用户
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/9
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 通过用户名查询用户
     *
     * @param username 用户名
     * @return 用户对象信息
     */
    SysUser selectUserByUserName(String username);

    /**
     * 根据id软删除系统用户
     * @param id  删除系统用户id
     * @param userId 更新者id
     */
    void deleteSysUser(Long id, Long userId);

    /**
     * 修改系统用户信息
     * @param sysUser
     * @param userId
     */
    void updateSysUser(SysUser sysUser, Long userId);

    /**
     * 根据id查询单个系统用户
     * @param id
     * @return
     */
    SysUser queryById(Long id);

    /**
     * 分页查询系统用户
     * @param pageQuerySysUser
     * @return
     */
    IPage<SysUser> pageQuery(PageQuerySysUser pageQuerySysUser);
}
