package com.sky.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sky.api.sys.param.response.SysLoginLogModel;
import com.sky.base.constant.DataStatus;
import com.sky.mapper.SysLoginLogMapper;
import com.sky.model.PageQueryModel;
import com.sky.model.SysLoginLog;
import com.sky.service.SysLoginLogService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/20
 */
@Service
public class SysLoginLogServiceImpl extends ServiceImpl<SysLoginLogMapper,SysLoginLog>
            implements SysLoginLogService {

    @Override
    public IPage<SysLoginLog> pageQuery(PageQueryModel pageQueryModel) {
        IPage<SysLoginLog> page = new Page<>(pageQueryModel.getPageNo(), pageQueryModel.getPageSize());
        LambdaQueryWrapper<SysLoginLog> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysLoginLog::getStatus, DataStatus.NORMAL);
        IPage<SysLoginLog> iPage = this.baseMapper.selectPage(page, queryWrapper);
        Assert.isTrue(iPage.getTotal()>0,"当前无数据");
        return iPage;
    }
}
