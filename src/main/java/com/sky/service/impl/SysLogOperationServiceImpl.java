package com.sky.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sky.mapper.SysLogOperationMapper;
import com.sky.model.SysLogOperation;
import com.sky.service.SysLogOperationService;
import org.springframework.stereotype.Service;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/5
 */
@Service
public class SysLogOperationServiceImpl extends ServiceImpl<SysLogOperationMapper, SysLogOperation>
            implements SysLogOperationService {
}
