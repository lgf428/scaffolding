package com.sky.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sky.api.sys.param.request.PageQuerySysUser;
import com.sky.base.constant.DataStatus;
import com.sky.mapper.SysUserMapper;
import com.sky.model.SysUser;
import com.sky.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDateTime;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/9
 */
@Service
@Slf4j
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper,SysUser>
        implements ISysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 通过用户名查询用户
     *
     * @param username 用户名
     * @return 用户对象信息
     */
    @Override
    public SysUser selectUserByUserName(String username) {
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUser::getUsername,username);
        return this.baseMapper.selectOne(queryWrapper);
    }

    /**
     * 根据id软删除系统用户
     * @param id  删除系统用户id
     * @param userId 更新者id
     */
    @Override
    public void deleteSysUser(Long id, Long userId) {
        LambdaUpdateWrapper<SysUser> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(SysUser::getStatus, DataStatus.NORMAL)
                .eq(SysUser::getId,id)
                .set(SysUser::getStatus,DataStatus.REMOVE)
                .set(SysUser::getUpdateId,userId)
                .set(SysUser::getUpdateTime, LocalDateTime.now());
        Assert.isTrue(this.update(updateWrapper),"删除失败");
    }

    /**
     * 修改系统用户信息
     * @param sysUser
     * @param userId
     */
    @Override
    public void updateSysUser(SysUser sysUser, Long userId) {
        LambdaUpdateWrapper<SysUser> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(SysUser::getStatus,DataStatus.NORMAL)
                .eq(SysUser::getId,sysUser.getId())
                .set(SysUser::getUpdateId,userId)
                .set(SysUser::getUpdateTime,LocalDateTime.now());
        Assert.isTrue(this.update(sysUser,updateWrapper),"修改失败");
    }

    /**
     * 根据id查询单个系统用户
     * @param id
     * @return
     */
    @Override
    public SysUser queryById(Long id) {
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUser::getStatus,DataStatus.NORMAL)
                .eq(SysUser::getId,id);
        SysUser sysUser = this.baseMapper.selectOne(queryWrapper);
        Assert.notNull(sysUser,"当前无数据");
        return sysUser;
    }

    /**
     * 分页查询系统用户
     * @param pageQuerySysUser
     * @return
     */
    @Override
    public IPage<SysUser> pageQuery(PageQuerySysUser pageQuerySysUser) {
        IPage<SysUser> iPage = new Page<>(pageQuerySysUser.getPageNo(), pageQuerySysUser.getPageSize());
        IPage<SysUser> page = sysUserMapper.pageQuery(iPage,pageQuerySysUser);
        Assert.isTrue(page.getTotal()>0,"当前无数据");
        return page;
    }


}
