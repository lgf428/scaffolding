package com.sky.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sky.model.PageQueryModel;
import com.sky.model.SysLoginLog;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/20
 */
public interface SysLoginLogService extends IService<SysLoginLog> {

    /** 分页查询登录日志 */
    IPage<SysLoginLog> pageQuery(PageQueryModel pageQueryModel);
}
