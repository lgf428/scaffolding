package com.sky.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sky.model.SysLogOperation;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/5
 */
public interface SysLogOperationService extends IService<SysLogOperation> {
}
