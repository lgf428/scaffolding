package com.sky.exception;

import com.sky.base.constant.ErrorCode;
import com.sky.utils.MessageUtils;

/**
 * 基础过滤器
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/13
 */
public class BaseException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    private int code;
    private String msg;

    public BaseException(int code) {
        this.code = code;
        this.msg = MessageUtils.getMessage(code);
    }

    public BaseException(int code, String... params) {
        this.code = code;
//        this.msg = MessageUtils.getMessage(code, params);
        this.msg = MessageUtils.getMessage(code, params);
    }

    public BaseException(int code, String msg) {
        super(msg);
        this.code = code;
//        this.msg = MessageUtils.getMessage(code, params);
        this.msg = msg;
    }

    public BaseException(int code, Throwable e) {
        super(e);
        this.code = code;
        this.msg = MessageUtils.getMessage(code);
    }

    public BaseException(int code, Throwable e, String... params) {
        super(e);
        this.code = code;
        this.msg = MessageUtils.getMessage(code, params);
    }

    public BaseException(String msg) {
        super(msg);
        this.code = ErrorCode.INTERNAL_SERVER_ERROR;
        this.msg = msg;
    }

    public BaseException(String msg, Throwable e) {
        super(msg, e);
        this.code = ErrorCode.INTERNAL_SERVER_ERROR;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
