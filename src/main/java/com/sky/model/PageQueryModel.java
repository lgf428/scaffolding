package com.sky.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/20
 */
@Data
@ApiModel
public class PageQueryModel implements Serializable {
    @ApiModelProperty("页码")
    private Integer pageNo;
    @ApiModelProperty("页面大小")
    private Integer pageSize;
}
