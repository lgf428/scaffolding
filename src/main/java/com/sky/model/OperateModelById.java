package com.sky.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/2
 */
@Data
@ApiModel("根据主键id删除")
public class OperateModelById {

    @ApiModelProperty(value = "id",required = true)
    @NotNull(message = "id不能为空")
    private Long id;

}
