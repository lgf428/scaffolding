package com.sky.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 同类,每个表都有这几个字段
 * 
 * @author 尹稳健~
 */
@Data
public class BaseEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 帐号状态（0正常 1停用） */
    @ApiModelProperty("帐号状态（0正常 1停用）")
    private Short status;

    /** 创建者 */
    @ApiModelProperty("创建者")
    private Long createId;

    /** 创建时间 */
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    /** 更新者 */
    @ApiModelProperty("更新者")

    private Long updateId;

    /** 更新时间 */
    @ApiModelProperty("更新时间")

    private LocalDateTime updateTime;

    /** 备注 */
    @ApiModelProperty("备注")
    private String remark;

}
