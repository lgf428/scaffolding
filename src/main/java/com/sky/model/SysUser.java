package com.sky.model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;

/**
 * 用户对象 sys_user
 * 
 * @author 尹稳健~
 */

@TableName("sys_user")
@Getter
@Setter
@ToString
public class SysUser extends BaseEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    @TableId
    private Long id;

    /** 用户账号 */
    private String username;

    /** 密码 */
    private String password;

    /** 用户昵称 */
    private String nickName;

    /** 用户头像 */
    private String avatar;

    /** 用户性别 */
    private Short sex;

    /** 用户邮箱 */
    private String email;

    /** 手机号码 */
    private String phone;

    /** 用户等级 */
    private Short hierarchy;

}
