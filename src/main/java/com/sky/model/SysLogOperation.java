package com.sky.model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 操作日志
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/5
 */
@Data
@TableName("sys_operation_log")
public class SysLogOperation extends BaseEntity implements Serializable {
    /** id */
    @TableId
    private Long id;
    /** 操作描述 */
    private String operation;
    /** 请求URI */
    private String requestUri;
    /** 请求方式 */
    private String requestMethod;
    /** 请求参数 */
    private String requestParams;
    /** 请求时长(毫秒) */
    private Integer requestTime;
    /** 操作IP */
    private String ip;
    /** 用户名 */
    private String username;
}
