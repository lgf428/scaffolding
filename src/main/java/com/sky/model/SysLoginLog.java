package com.sky.model;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/8/15
 */
@Data
@TableName("sys_login_log")
public class SysLoginLog implements Serializable {

    @TableId
    private Long id;
    /** 状态 0：成功 1:失败 */
    private Short status;
    /** IP地址 */
    private String ip;
    /** 用户名 */
    private String username;
    /** 创建者 */
    private Long createId;
    /** 登录时间 */
    private LocalDateTime loginTime;
}
