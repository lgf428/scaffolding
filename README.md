Springboot简化开发脚手架
项目文档
简介
后端采用SpringBoot、SpringSecurity、Redis&Jwt
数据层采用Mybatis-Plus和Mybatis，接口文档使用Swagger3
这是一个简单的框架脚手架，已经有了全局异常捕获

内置功能
1.系统用户：完成系统用户的增删改查

2.登录日志：用户登录日志记录

3.操作日志：采用AOP，可以进行查询查看

4.系统接口：根据业务代码生成相关的api接口文档

5.连接池监视：采用阿里的druid

技术选型
1.系统环境：

jdk1.8
servlet3.0
Apache Maven 3.6
2.主框架

SpringBoot 2.7.2
SpringFramework 5.3.2
SpringSecurity 5.7.2
3.持久层

MybatisPlus 3.5.2
Alibaba druid 1.2.11
hibernate - validator 6.3.2
配置Swagger 3
1.导入依赖jar包
<!-- web服务 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<!--swagger3-->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-boot-starter</artifactId>
    <version>3.0.0</version>
</dependency>
2.配置SwaggerConfig
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/6
 */
@Configuration
@EnableOpenApi
public class SwaggerConfig {

    /**
     * 创建API
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.OAS_30)
                // 是否启用swagger
                .enable(true)
                //用来创建该API的基本信息，展示在文档的页面中（自定义展示的信息）
                .apiInfo(apiInfo())
                // 设置哪些接口暴露给Swagger展示
                .select()
                // 扫描所有有注解的api，用这种方式更灵活
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                // 扫描所有包中的swagger注解
                .paths(PathSelectors.any())
                .build();

    }

    /**
     * 添加摘要信息
     */
    public ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                // 设置标题
                .title("标题：尹稳健管理系统_Swagger3接口文档")
                // 描述
                .description("尹稳健无敌第一帅~")
                .contact(new Contact("yinwenjian", null, null))
                // 作者信息
                .version("v1.0")
                // 版本
                .build();
    }
}
3.直接启动（埋雷）
org.springframework.context.ApplicationContextException: Failed to start bean 'documentationPluginsBootstrapper'; nested exception is java.lang.NullPointerException
这里我们发现启动报错了，解决方案:

在application.yml中配置

# Springfox使用的路径匹配是基于AntPathMatcher的，
#而Spring Boot 2.6.X使用的是PathPatternMatcher。
spring:
	mvc:
		pathmatch:
			matching-strategy: ant_path_matcher
运行成功！访问 http://localhost:8080/sky/swagger-ui/ 得到：

如果你没有写接口的话那么你肯定是比我少一个==测试接口类==的，下面是我的代码：

package com.sky.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/6
 */
@Api(tags = "测试接口类")
@RestController
public class HelloController {

    @ApiOperation("测试hello")
    @PostMapping("/hello")
    public String hello(){
        return "hello";
    }
}
额。。。说实话，我感觉swagger3自带的这个页面挺丑的，所以我们可以给他改个样式：

4.修改swagger3前端样式：
<!--swagger3 ui 界面 个人喜好-->
<dependency>
    <groupId>com.github.xiaoymin</groupId>
    <artifactId>knife4j-spring-ui</artifactId>
    <version>3.0.3</version>
</dependency>
访问 http://localhost:8080/sky/doc.html 得到

总结：
swagger3配置SpringBoot高版本会出现错误，只需要在yml配置中加入配置
swagger3默认的访问地址是 http://localhost:8080/sky/swagger-ui/ 改了他的样式后，访问的地址是 http://localhost:8080/sky/doc.html
5.扩展
配置swagger发送请求每次携带token，因为一般后端项目都有用到安全框架，比如现在很火的SpringSecurity或者Shiro

在swagger配置中配置：

package com.sky.config;

import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/6
 */
@Configuration
@EnableOpenApi
public class SwaggerConfig {

    /**
     * 创建API
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.OAS_30)
                // 是否启用swagger
                .enable(true)
                //用来创建该API的基本信息，展示在文档的页面中（自定义展示的信息）
                .apiInfo(apiInfo())
                // 设置哪些接口暴露给Swagger展示
                .select()
                // 扫描所有有注解的api，用这种方式更灵活
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                // 扫描所有包中的swagger注解
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts());

    }

    /**
     * 添加摘要信息
     */
    public ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                // 设置标题
                .title("标题：尹稳健管理系统_Swagger3接口文档")
                // 描述
                .description("尹稳健无敌第一帅~")
                .contact(new Contact("yinwenjian", null, null))
                // 作者信息
                .version("v1.0")
                // 版本
                .build();
    }

    /**
     * 安全模式，这里指定token通过Authorization头请求头传递
     * 通过 securitySchemes 来配置全局参数，这里的配置是一个名为 Authorization 的请求头（OAuth2 中需要携带的请求头）。
     */
    private List<SecurityScheme> securitySchemes() {
        List<SecurityScheme> apiKeyList = new ArrayList<SecurityScheme>();
        apiKeyList.add(new ApiKey("Authorization", "Authorization", In.HEADER.toValue()));
        return apiKeyList;
    }

    /**
     * 安全上下文
     * securityContexts 则用来配置有哪些请求需要携带 Token，这里我们配置了所有请求
     */
    private List<SecurityContext> securityContexts() {
        List<SecurityContext> securityContexts = new ArrayList<>();
        securityContexts.add(
                SecurityContext.builder()
                        .securityReferences(defaultAuth())
                        .operationSelector(o -> o.requestMappingPattern().matches("/.*"))
                        .build());
        return securityContexts;
    }

    /**
     * 默认的安全上引用
     */
    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        List<SecurityReference> securityReferences = new ArrayList<>();
        securityReferences.add(new SecurityReference("Authorization", authorizationScopes));
        return securityReferences;
    }
}
测试类接口：

package com.sky.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/6
 */
@Api(tags = "测试接口类")
@RestController
@Slf4j
public class HelloController {

    @ApiOperation("测试hello")
    @PostMapping("/hello")
    public String hello(HttpServletRequest request){
        String authorization = request.getHeader("Authorization");
        log.info(authorization);
        return "hello";
    }
}
运行：

然后尝试调用controller接口

查看控制台打印

2022-09-06 14:56:55.147  INFO 21812 --- [nio-8080-exec-7] com.sky.controller.HelloController       : Bearer eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwiaWF0IjoxNjYyMjc1MjM1LCJleHAiOjE2NjIyNzcwMzV9.VFMdI_VjKL0nHJOWcEeOR8b0sUCXdQoqfJD3uUP5VoU
能获取到token信息。这样swagger3的配置就全部完成了。谢谢能看到最后！

SpringSecurity最新配置
博客简介
本文章也是完成适合小白的，我会站在一个小白的立场来写这篇博客的，因为SpringSecurity的更新，已经启用了以前继承的写法，所以我写这篇博客来记录学习，也是尽可能的将一些知识融会贯通到这个里面来，比如说JWT，Redis等等，如果有错误，可以在评论区指出，因为大家都是学习者，没有完全的对错之分，谢谢大家！

​ ==如果你什么都没写，那么我建议你先搭建一个简单的SpringBoot项目，不然很多效果你会看不到==

​ 或者你可以看我SpringBoot如何配置Swagger3这篇博客，因为这篇博客是在上一篇博客的基础之上写的，你也可以认为这篇文章是上一篇文章的后续

1.导入相关依赖
这里导入redis和mybatis用来连接数据库，jwt可以存入redis中

<!-- SpringSecurity 依赖 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
<!-- redis连接 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
<!--SpringBoot集成Mybatis-plus-->
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.5.2</version>
</dependency>
<!-- 生成和解析token-->
<dependency>
    <groupId>io.jsonwebtoken</groupId>
    <artifactId>jjwt-api</artifactId>
    <version>0.11.2</version>
</dependency>
<dependency>
    <groupId>io.jsonwebtoken</groupId>
    <artifactId>jjwt-impl</artifactId>
    <version>0.11.2</version>
</dependency>
<dependency>
    <groupId>io.jsonwebtoken</groupId>
    <artifactId>jjwt-jackson</artifactId>
    <version>0.11.1</version>
</dependency>
导入Springsecurity后我们访问接口会发现，弹出了一个界面

这个时候我们需要看下开启项目后的控制台，里面会有一个SpringSecurity生成的密码

输入账号：user

密码：项目启动后的控制台里面会有输出的

输入完账号和密码后，会跳转到你之前输入地址的页面，因为我的访问方式是post，所以没有展示出来

这个时候如果我们配置了Swagger也是不能访问的，那我们该怎么去设置让他放行Swagger页面，不需要登录也能访问呢？

2.SpringSecurity配置（重点）
配置一些哪些路径应该放行，我们配置了SpringSecurity那么就不会弹出需要登录

package com.sky.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/7
 */
@Configuration
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return  http
                //关闭csrf
                .csrf().disable()
                //不通过Session获取SecurityContext
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // 允许跨域
                .cors()
                .and()
                // 配置路劲是否需要认证
                .authorizeRequests()
                // 对于登录接口 允许匿名访问 anonymous 仅允许匿名用户访问,如果登录了访问 反而没权限
                .antMatchers("/login").permitAll()
                // 静态资源，可匿名访问
                .antMatchers(HttpMethod.GET, "/", "/*.html", "/**/*.html", "/**/*.css", "/**/*.js", "/profile/**","/*.ico").permitAll()
                .antMatchers("/swagger-ui.html","/doc.html/**", "/swagger-resources/**", "/webjars/**", "/*/api-docs", "/druid/**","/doc.html").permitAll()
                // 除上面外的所有请求全部需要鉴权认证
                .anyRequest().fullyAuthenticated()
                .and()
                .build();
    }
}
编写登录接口
准备响应类：

package com.sky.utils;

import java.io.Serializable;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/7
 */
public class R<T> implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 成功 */
    public static final int SUCCESS = 200;

    /** 失败 */
    public static final int FAIL = 500;

    private int code;

    private String msg;

    private T data;

    public static <T> R<T> ok()
    {
        return restResult(null, SUCCESS, "操作成功");
    }

    public static <T> R<T> ok(T data)
    {
        return restResult(data, SUCCESS, "操作成功");
    }

    public static <T> R<T> ok(T data, String msg)
    {
        return restResult(data, SUCCESS, msg);
    }

    public static <T> R<T> fail()
    {
        return restResult(null, FAIL, "操作失败");
    }

    public static <T> R<T> fail(String msg)
    {
        return restResult(null, FAIL, msg);
    }

    public static <T> R<T> fail(T data)
    {
        return restResult(data, FAIL, "操作失败");
    }

    public static <T> R<T> fail(T data, String msg)
    {
        return restResult(data, FAIL, msg);
    }

    public static <T> R<T> fail(int code, String msg)
    {
        return restResult(null, code, msg);
    }

    private static <T> R<T> restResult(T data, int code, String msg)
    {
        R<T> apiResult = new R<>();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        return apiResult;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public T getData()
    {
        return data;
    }

    public void setData(T data)
    {
        this.data = data;
    }
}
接收前端数据的实体类

package com.sky.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 尹稳健~
 * @version 1.0
 * @time 2022/9/7
 */
@ApiModel("用户登录入参模型")
@Data
public class LoginBody {
    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名",required = true)
    private String username;

    /**
     * 用户密码
     */
    @ApiModelProperty(value = "用户密码",required = true)
    private String password;

}